/**
 * Created by alex on 17.03.17.
 */
import React, {Component} from "react";
import MessagesToolbar from "../messagesToolbar/messages";
import NotificationToolBar from "../messagesToolbar/notifications";
export default class RightNavBar extends Component{
    render(){
        return(
            <ul className="nav navbar-nav navbar-right top-menu top-right-menu">
                {this.props.messages && <MessagesToolbar/>}
                {this.props.notifications && <NotificationToolBar/>}
            </ul>
        )
    }
}
RightNavBar.propTypes = {
    messages: React.PropTypes.bool.isRequired,
    notifications: React.PropTypes.bool.isRequired
};