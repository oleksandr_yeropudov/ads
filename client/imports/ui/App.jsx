import React, {Component} from "react";
import Aside from "./aside/aside.jsx";
import Content from "./content/content.jsx";
import Logo from "./aside/logo.jsx";
import Dashboard from "./dashboard/dashboard.jsx";
export default class App extends Component {

    constructor() {
        super();
        this.handleMenuToggle = this.handleMenuToggle.bind(this);
        this.state = {
            menuToggled: false
        }
    }
    handleMenuToggle(){
        this.setState({
            menuToggled: !this.state.menuToggled
        });
    }
    renderComponent(){
        return <Dashboard menuToggle={this.handleMenuToggle}/>
    }
    renderAside() {
        return <Aside toggled={this.state.menuToggled} />
    }

    renderContent() {
        return <Content contentComponent={this.renderComponent()}/>
    }

    renderLogo() {
        return <Logo/>
    }

    render() {
        let menuToggled = (this.state.menuToggled)? 'left-panel collapsed' : 'left-panel';
        return (
            <main>
                <aside className={menuToggled}>
                    { this.renderLogo() }
                    { this.renderAside() }
                </aside>
                <section className="content">
                    {this.renderContent()}
                </section>
            </main>
        );
    }
}