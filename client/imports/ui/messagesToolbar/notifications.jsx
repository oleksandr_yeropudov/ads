/**
 * Created by alex on 17.03.17.
 */
import React, {Component, PropTypes} from "react";
import NotificationItem from "./notificationItem";
import { createContainer } from 'meteor/react-meteor-data';
import { Notifications } from '../../../../imports/api/Notifications';

class NotificationToolBar extends Component{
    renderNotifications(){
        return this.props.notifications
            .map(function(notification) {
                //Meteor.call('notifications.markRead', notification._id);
                return <NotificationItem key={notification._id} text={notification.text} time={notification.time}/>
            });
    }
    render(){
        return(
            <li className="dropdown">
                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                    <i className="fa fa-bell-o"></i>
                    <span className="badge badge-sm up bg-pink count">3</span>
                </a>
                <ul className="dropdown-menu extended fadeInUp animated nicescroll" tabIndex="5002">
                    <li className="noti-header">
                        <p>Notifications</p>
                    </li>
                    {this.renderNotifications()}
                    <li>
                        <p><a href="#" className="text-right">See all notifications</a></p>
                    </li>
                </ul>
            </li>
        )
    }
}
NotificationToolBar.propTypes = {
    notifications: PropTypes.array.isRequired
};
export default createContainer(()=> {
    Meteor.subscribe('notifications');
    return{
        notifications: Notifications.find({new: true}).fetch(),
    };
}, NotificationToolBar);