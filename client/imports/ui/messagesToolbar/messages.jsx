/**
 * Created by alex on 17.03.17.
 */
import React, {Component, PropTypes} from "react";
import MessageItem from "./messageItem";
import { createContainer } from 'meteor/react-meteor-data';
import {Messages} from  "../../../../imports/api/Messages";

class MessagesToolbar extends Component{
    renderMessages(){
        return this.props.messages.map((message) => (
            <MessageItem key={message._id} avatar={message.avatar} from={message.from} text={message.text} time={message.time}/>
        ));
    }

    render(){
        return(
            <li className="dropdown">
                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                    <i className="fa fa-envelope-o "></i>
                    <span className="badge badge-sm up bg-purple count">4</span>
                </a>
                <ul className="dropdown-menu extended fadeInUp animated nicescroll" tabIndex="5001">
                    <li>
                        <p>Messages</p>
                    </li>
                    {this.renderMessages()}
                    <li>
                        <p><a href="inbox.html" className="text-right">See all Messages</a></p>
                    </li>
                </ul>
            </li>
        )
    }
}

MessagesToolbar.PropsTypes = {
    messages: PropTypes.array.isRequired
};

export default createContainer(()=> {
    return{
        messages: Messages.find({}).fetch(),
    };
}, MessagesToolbar);