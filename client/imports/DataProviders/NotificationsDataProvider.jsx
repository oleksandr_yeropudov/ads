/**
 * Created by alex on 17.03.17.
 */
export default class NotificationsDataProvider {
    fetchNotifications(){
        return [
            {
                _id: 1,
                text: "Some new Notification",
                time: new Date(2017, 2, 17, 17, 40, 20, 0, 0)
            },
            {
                _id: 2,
                text: "Some old Notification",
                time: new Date(2017, 2, 17, 17, 30, 20, 0, 0)
            }
        ]
    }
};