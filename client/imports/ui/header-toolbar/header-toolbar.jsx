/**
 * Created by alex on 17.03.17.
 */
import React, {Component, PropTypes} from "react";
import LeftNavBar from  "./leftnav-bar";
import RightNavBar from "./right-navbar";

export default class HeaderToolbar extends Component {

    render(){
        return (
            <header className="top-head container-fluid">
                <button type="button" className="navbar-toggle pull-left" onClick={this.props.toggleMenuClick}>
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
                <form role="search" className="navbar-left app-search pull-left hidden-xs">
                    <input type="text" placeholder="Search..." className="form-control" />
                        <a href=""><i className="fa fa-search"></i></a>
                </form>
                <nav className="navbar-default" role="navigation">
                    <LeftNavBar/>
                    <RightNavBar notifications={true} messages={true}/>
                </nav>
            </header>
        )
    }
}