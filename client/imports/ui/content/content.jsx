/**
 * Created by alex on 03.03.17.
 */
import React, {Component, PropTypes} from "react";
import NotifyContainerTopLeft from "../notify-containers/top-left";
import NotifyContainerTopRight from "../notify-containers/top-right";
import NotifyContainerTopCenter from "../notify-containers/top-center";
import NotifyContainerLeftMiddle from "../notify-containers/left-middle";
import NotifyContainerRightMiddle from "../notify-containers/right-middle";
import NotifyContainerBottomCenter from "../notify-containers/bottom-center";
import NotifyContainerBottomLeft from "../notify-containers/bottom-left";
import NotifyContainerBottomRight from "../notify-containers/bottom-right";


export default class Content extends Component {
    componentDidMount() {
        let _this = this;
        setTimeout(function () {
            if(typeof initNotiFicationSMetro === "function"){
                initNotiFicationSMetro();
            } else {
                _this.componentDidMount();
            }
        }, 300);
    }
    handleFlachClick(){

    }
    render() {
        return (
            <main>
                {this.props.contentComponent}
                <NotifyContainerTopLeft/>
                <NotifyContainerTopRight/>
                <NotifyContainerTopCenter/>

                <NotifyContainerLeftMiddle/>
                <NotifyContainerRightMiddle/>

                <NotifyContainerBottomCenter/>
                <NotifyContainerBottomLeft/>
                <NotifyContainerBottomRight/>
            </main>
        );
    }
}
Content.propTypes = {
    contentComponent: PropTypes.node.isRequired,
};