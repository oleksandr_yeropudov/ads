/**
 * Created by alex on 14.03.17.
 */
import React, {Component, PropTypes} from "react";
import HeaderToolbar from "../header-toolbar/header-toolbar";
export default class Dashboard extends Component {
    renderHeaderBar(){
        return <HeaderToolbar toggleMenuClick = {this.props.menuToggle}/>
    }
    render() {
        return (
            this.renderHeaderBar()
        );
    }
}