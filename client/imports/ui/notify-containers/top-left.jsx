/**
 * Created by alex on 30.03.17.
 */
import React, {Component, PropTypes} from "react";
import { createContainer } from 'meteor/react-meteor-data';
import  FlashNotification  from '../flash-notifications/flash-notification';
import { FlashNotifications } from '../../../../imports/api/FlashNotifications';
class NotifyContainerTopLeft extends Component {

    renderFlash()
    {
        return this.props.flash.map(function(flash) {
                return <FlashNotification id={flash._id} key={flash._id} icon={flash.icon} type={flash.type} title={flash.title} message={flash.message} prompt={flash.prompt} autohide={flash.autohide}/>
            }
        );
    }

    render() {
        return(
            <div className="notifyjs-corner" style={{'top': '0px', 'left': '0px'}}>
                { this.renderFlash()}
            </div>
        );
    }
}
export default createContainer(()=> {
    Meteor.subscribe('flash-notifications');
    return{
        flash: FlashNotifications.find({"position":"top-left"}).fetch(),
    };
}, NotifyContainerTopLeft);