/**
 * Created by alex on 17.03.17.
 */
import React, {Component} from "react";
export default class LeftNavBar extends Component {
    render(){
        return(
            <ul className="nav navbar-nav hidden-xs">
                <li className="dropdown">
                    <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                        English
                        <span className="caret"></span>
                    </a>
                    <ul role="menu" className="dropdown-menu">
                        <li><a href="#">German</a></li>
                        <li><a href="#">French</a></li>
                        <li><a href="#">Italian</a></li>
                        <li><a href="#">Spanish</a></li>
                    </ul>
                </li>
            </ul>
        )
    }

}