/**
 * Created by alex on 03.03.17.
 */
import {Template} from "meteor/templating";
import "./body.html";
let aside = require('./aside/aside');
let content = require('./content/content');

Template.body.helpers({
    aside: aside,
    content: content,
});