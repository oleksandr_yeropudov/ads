/**
 * Created by alex on 17.03.17.
 */
import React, {Component, PropTypes} from "react";
import DateHelper from "../../Helpers/DateHelper";

export default class MessageItem extends Component{

    render() {
        return (
            <li>
                <a href="#">
                    <span className="pull-left">
                        <img src={this.props.avatar} className="img-circle thumb-sm m-r-15" alt={this.props.from}/>
                    </span>
                    <span className="block">
                        <strong>{this.props.from}</strong>
                    </span>
                    <span className="media-body block">
                        {this.props.text}
                        <br />
                        <small className="text-muted">{new DateHelper().formatDate(new Date().getTime() - this.props.time.getTime())}</small>
                    </span>
                </a>
            </li>
        );
    }
}
MessageItem.propTypes = {
    avatar: React.PropTypes.string.isRequired,
    from: React.PropTypes.string.isRequired,
    text: React.PropTypes.string.isRequired,
    time: React.PropTypes.object.isRequired
}