/**
 * Created by alex on 22.03.17.
 */
import { Mongo } from "meteor/mongo";

export const FlashNotifications = new Mongo.Collection('flash-notifications');

if (Meteor.isServer) {
    Meteor.publish('flash-notifications', function flashPublication() {
        return FlashNotifications.find({});
    });
}
Meteor.methods({
    'flash-notifications.markRead'(notificationId){
        FlashNotifications.update(notificationId, {$set: {position: ''}});
    },
});