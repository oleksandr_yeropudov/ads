/**
 * Created by alex on 09.03.17.
 */
const Menus = [
    {
        _id: 1,
        type: 'menu',
        hasChild: true,
        url: '#',
        hasIcon: true,
        icon: 'ion-flask',
        label: 'UI Elements',
        active: false,
        subMenus: [
            {
                _id: '1_1',
                type: 'submenu',
                url: "#",
                label: 'Submenu',
                active: true,
            },
            {
                _id: '1_2',
                type: 'submenu',
                url: "#",
                label: 'Submenu2',
                active: false,
            },
        ]
    },
    {
        _id: 2,
        type: 'menu',
        hasChild: false,
        url: '#',
        hasIcon: true,
        icon: 'ion-flask',
        label: 'UI Elements2',
        active: false,
        subMenus: []
    }
];