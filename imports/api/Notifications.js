/**
 * Created by alex on 20.03.17.
 */
import { Mongo } from "meteor/mongo";

export const Notifications = new Mongo.Collection('notifications');

if (Meteor.isServer) {
    Meteor.publish('notifications', function notificationPublication() {
        return Notifications.find({new: true});
    });
}

Meteor.methods({
   'notifications.markRead'(notificationId){
       Notifications.update(notificationId, {$set: {new: false}});
   },
});