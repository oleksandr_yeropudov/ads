/**
 * Created by alex on 17.03.17.
 */
import React, {Component, PropTypes} from "react";
import DateHelper from "../../Helpers/DateHelper";

export default class NotificationItem  extends Component {
    render() {
        return(
            <li>
                <a href="#">
                    <span className="pull-left"><i className="fa fa-user-plus fa-2x text-info"></i></span>
                    <span>
                        {this.props.text}
                        <br/>
                        <small className="text-muted">
                            {new DateHelper().formatDate(new Date().getTime() - this.props.time.getTime())}
                        </small>
                    </span>
                </a>
            </li>
        )
    }
};

NotificationItem.propTypes = {
    text: React.PropTypes.string.isRequired,
    time: React.PropTypes.object.isRequired
}