/**
 * Created by alex on 17.03.17.
 */
export default class MessagesDataProvider {
    fetchMessages(){
        return [
            {
                _id: 1,
                from: '11111',
                text: '1111 Some long text.Some long text.Some long text.Some long text.Some long text.Some long text.',
                time: new Date(2017, 2, 17, 16, 33, 0, 0),
                avatar: 'img/avatar-2.jpg'
            },
            {
                _id: 2,
                from: '22222',
                text: '22222 Some long text.Some long text.Some long text.Some long text.Some long text.Some long text.',
                time: new Date(2017, 2, 17, 16, 50, 0, 0),
                avatar: 'img/avatar-3.jpg'
            },
        ]
    }
};
