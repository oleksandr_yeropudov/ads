/**
 * Created by alex on 06.03.17.
 */
import React, {Component, PropTypes} from "react";

export default class Logo extends Component {
    render() {
        return (
            <div className="logo">
                <a href="index.html" className="logo-expanded">
                    <i className="ion-social-buffer"></i>
                    <span className="nav-label">Velonic</span>
                </a>
            </div>
        );
    }
}
