/**
 * Created by alex on 22.03.17.
 */
import React, {Component, PropTypes} from "react";

export default class FlashNotification extends Component{
    handleClickFlash(){
        Meteor.call('flash-notifications.markRead', this.props.id);
    }
    renderPrompt(){
        if(this.props.prompt){
            return (
            <div>
                <div className="clearfix"></div>
                <br/><a className="btn btn-sm btn-default yes">Yes</a>
                <a className="btn btn-sm btn-danger no">No</a>
            </div>
            );
        }
        return false;
    }
    render() {
        return(
            <div className="notifyjs-wrapper" onClick={this.handleClickFlash.bind(this)}>
                <div className="notifyjs-arrow" style={{}}></div>
                <div className="notifyjs-container">
                    <div className={"notifyjs-metro-base notifyjs-metro-"+this.props.type}>
                        <div className="image" data-notify-html="image">
                                <i className={"fa fa-"+this.props.icon}></i>
                            </div>
                            <div className="text-wrapper">
                                <div className="title" data-notify-html="title">{this.props.title}</div>
                                <div className="text" data-notify-html="text">
                                    {this.props.message}
                                    {this.renderPrompt()}
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        );
    }
};
FlashNotification.propTypes = {
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    autohide: PropTypes.bool.isRequired,
    prompt: PropTypes.bool.isRequired,
    promptFunc: React.PropTypes.func,
    timer: React.PropTypes.number,
};