/**
 * Created by alex on 17.03.17.
 */
export default class DateHelper {
    formatDate(ms){
        ms = ms / 1000;
        const day = 86400;
        const min = 60;
        const hour = min*60;
        let minAgo=0, hoursAgo=0, daysAgo=0;
        if(ms >= day) {
            daysAgo = Math.floor(ms / day);
            if (daysAgo > 0) {
                ms -= day * daysAgo;
            }
        }
        if(ms >= hour) {
            hoursAgo = Math.floor(ms/hour);
            if(hoursAgo>0){
                ms -= hour*hoursAgo;
            }
        }
        if(ms>=min) {
            minAgo = Math.floor(ms/min);
            if(min > 0) {
                ms-=min*minAgo;
            }
        }
        let secAgo = Math.floor(ms);
        let timeStr='';
        if(hoursAgo>0){
            timeStr += hoursAgo + ' h ';
        }
        if(daysAgo>0) {
            timeStr+= daysAgo + ' d ';
        }
        if(minAgo>0) {
            timeStr += minAgo + ' min ';
        }
        timeStr += secAgo + ' sec ago';
        return timeStr;
    }
};