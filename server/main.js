import { Meteor } from 'meteor/meteor';
import  '../imports/api/Notifications';
import  '../imports/api/Messages';
import  '../imports/api/FlashNotifications';
Meteor.startup(() => {
  // code to run on server at startup
});
