/**
 * Created by alex on 06.03.17.
 */
import React, {Component, PropTypes} from "react";

export default class AsideElement extends Component {

    renderSubMenus() {
        let menus = this.props.menuElement.subMenus;
        return menus.map(
            (submenu) => (<AsideElement
                idis={[submenu._id, this.props.menuElement._id]}
                key={submenu._id}
                menuElement={submenu}
                active={submenu.active}
                type={submenu.type}
                onClick={this.props.onClick}/>)
        );
    }

    render() {
        let classLi = (this.props.active) ? 'active ' : 'in-active ';
        if (this.props.menuElement.type === 'menu') {

            if (this.props.menuElement.hasChild) {
                classLi += 'has-submenu';
            }
            let icon = (this.props.menuElement.hasIcon) ? this.props.menuElement.icon : '';

            if (!this.props.menuElement.hasChild) {
                return (
                    <li className={classLi} onClick={this.props.onClick.bind(null, this)}>
                        <a href={ this.props.menuElement.url }>
                            <i className={icon}></i>
                            <span className="nav-label">
                            {this.props.menuElement.label}
                        </span>
                        </a>
                    </li>
                );
            } else {
                return (
                    <li className={classLi} onClick={this.props.onClick.bind(null, this)}>
                        <a href={ this.props.menuElement.url }>
                            <i className={icon}></i>
                            <span className="nav-label">
                            {this.props.menuElement.label}
                        </span>
                        </a>
                        <ul className="list-unstyled">
                            {this.renderSubMenus()}
                        </ul>
                    </li>
                );
            }
        } else {
            return (
                <li className={classLi} onClick={this.props.onClick.bind(null, this)}><a
                    href={ this.props.menuElement.url }>{this.props.menuElement.label}</a></li>
            );
        }
    }
}

AsideElement.propTypes = {
    menuElement: PropTypes.object.isRequired,
};