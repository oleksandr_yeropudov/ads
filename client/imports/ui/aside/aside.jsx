/**
 * Created by alex on 03.03.17.
 */
import React, {Component, PropTypes} from "react";
import AsideMenu from "./aside-element.jsx";
export default class Aside extends Component {
    constructor() {
        super();
        this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
        this.menus = this.getMenuItems();
        this.state = {
            active: null,
            subMenusClick: false
        };
        this.submenusClick = false;
    }

    getMenuItems() {
        return [
            {
                _id: 1,
                type: 'menu',
                hasChild: true,
                url: '#',
                hasIcon: true,
                icon: 'ion-flask',
                label: 'UI Elements',
                active: false,
                subMenus: [
                    {
                        _id: '1_1',
                        type: 'submenu',
                        url: "#",
                        label: 'Submenu',
                        active: false,
                    },
                    {
                        _id: '1_2',
                        type: 'submenu',
                        url: "#",
                        label: 'Submenu2',
                        active: false,
                    },
                ]
            },
            {
                _id: 2,
                type: 'menu',
                hasChild: false,
                url: '#',
                hasIcon: true,
                icon: 'ion-flask',
                label: 'UI Elements2',
                active: false,
                subMenus: []
            }
        ];
    }

    renderMenu() {
        let _this = this;
        _this.submenusClick = false;
        return this.menus.map(function (item) {
            let activeState = false;
            if (_this.state.active != null) {
                if (_this.state.active.indexOf(item._id) != -1) {
                    activeState = true;
                }
                if (item.hasChild) {
                    for (let i = 0; i < item.subMenus.length; i++) {
                        if (_this.state.active.indexOf(item.subMenus[i]._id) != -1) {
                            item.subMenus[i].active = true;
                        } else {
                            item.subMenus[i].active = false;
                        }
                    }
                }
            }
            return <AsideMenu type={item.type} menuElement={item} idis={[item._id]} active={activeState} key={item._id}
                              onClick={_this.handleMenuItemClick}/>
        });
    }

    handleMenuItemClick(asidemenu, event) {
        console.log(this.submenusClick);
        if (!this.submenusClick) {
            this.setState({
                active: asidemenu.props.idis
            });
            if (asidemenu.props.type === 'submenu') {
                this.submenusClick = true;
            }
        }
    }

    render() {
        return (
            <nav className="navigation">
                <ul className="list-unstyled">
                    {this.renderMenu()}
                </ul>
            </nav>
        );
    }
}
